from django.contrib import admin
from django.urls import path, include
from .views import api_list_customers,api_detail_customer, api_list_sales, api_detail_sale, api_list_salespeople, api_detail_salesperson

urlpatterns = [
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:id>/", api_detail_customer, name="api_detail_customer"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:id>/", api_detail_sale, name="api_detail_sale"),
    path("salespeople/", api_list_salespeople, name="api_list_salespersons"),
    path("salespeople/<int:id>/", api_detail_salesperson, name="api_detail_salesperson"),
]
