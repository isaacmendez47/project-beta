from django.db import models

class AutomobileVO(models.Model):
    ##color = models.CharField(max_length=20)
    ##year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, )
    sold = models.BooleanField(default=False)
    import_href=models.CharField(max_length=200)
    vip = models.BooleanField(default=False)

    ##def __str__(self):
        ##return self.vin

class Technician(models.Model):
    first_name = models.CharField(max_length = 100)
    last_name = models.CharField(max_length = 100)
    employee_id = models.CharField(max_length=20)

class Appointment(models.Model):
    date_time = models.DateTimeField(blank=True)
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name='appointments',
        on_delete=models.CASCADE
    )



# Create your models here.
