# Generated by Django 4.0.3 on 2023-06-08 18:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0003_technician_appointment'),
    ]

    operations = [
        migrations.AddField(
            model_name='automobilevo',
            name='vip',
            field=models.BooleanField(default=False),
        ),
    ]
