import React, {useEffect, useState} from 'react'



function TechList(){
	const [technicians, setTechnicians] = useState([])
	const fetchData = async () =>{
		const url = 'http://localhost:8080/api/technicians/'
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setTechnicians(data.technicians)
		}
	}

	useEffect(() => {
		fetchData();
	},[])

	const deleteTechnician = async(id) => {
		const url = `http://localhost:8080/api/technician/${id}`
		const response = await fetch(url, {method: "DELETE"});
		if (response.ok){
			setTechnicians(technicians.filter((technician) => technician.id !==id));
		}
	}






    return(
		<div>
         <h1>Technicians</h1>
			<table className ="table table_stripped">
				<thead>
					<tr>
						<th>Employee ID</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>ID</th>
					</tr>
				</thead>
				<tbody>
					{technicians?.map(technician => {
						return(
							<tr key={technician.id}>
								<td>{technician.employee_id}</td>
								<td>{technician.first_name}</td>
								<td>{technician.last_name}</td>
								<td>{technician.id}</td>
								<td><button className= 'btn btn-danger' onClick={() => deleteTechnician(technician.id)}> Delete Technician</button></td>
							</tr>
						)
					})}

				</tbody>
			</table>
		</div>
    )









}


export default TechList;
