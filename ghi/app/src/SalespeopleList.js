import React from 'react'
import { useEffect, useState} from 'react'

function Salespeoplelist () {
	const [salespeople, setSalespeople] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople)
    }
  }

	useEffect(() => {
    fetchData();
  }, []);

	const deleteSalespeople = async(id) => {
		const salespersonIdUrl = `http://localhost:8090/api/salespeople/${id}`
		const response = await fetch(salespersonIdUrl, {method:"DELETE"});
		if (response.ok){
			setSalespeople(salespeople.filter((salespeople)=> salespeople.id !==id));
		}
	}

	return (
		<div>
			<h1>Salespeople</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Employee id</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					{salespeople?.map(salespeople => {
						return (
							<tr key={salespeople.id}>
								<td>{ salespeople.employee_id }</td>
								<td>{ salespeople.first_name }</td>
								<td>{ salespeople.last_name }</td>
								<td>
									<button className= "btn btn-danger" onClick={() => deleteSalespeople(salespeople.id)}>Fire</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}


export default Salespeoplelist
