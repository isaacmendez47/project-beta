import React from 'react'
import { useEffect, useState} from 'react'


function CustomerList() {
	const [customers, setCustomers] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers)
    }
  }

	useEffect(() => {
    fetchData();
  }, []);

	const deleteCustomer = async(id) => {
		const customerIdUrl = `http://localhost:8090/api/customers/${id}`
		const response = await fetch(customerIdUrl, {method:"DELETE"});
		if (response.ok){
			setCustomers(customers.filter((customer)=> customer.id !==id));
		}
	}

	return (
		<div>
			<h1>Customers</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>First Name</th>
						<th>Last name</th>
						<th>Address</th>
						<th>Phone Number</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					{customers?.map(customers => {
						return (
							<tr key={customers.id}>
								<td>{ customers.first_name }</td>
								<td>{ customers.last_name }</td>
								<td>{ customers.address }</td>
								<td>{ customers.phone_number }</td>
								<td>
									<button className= "btn btn-danger" onClick={() => deleteCustomer(customers.id)}>Remove</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}

export default CustomerList
