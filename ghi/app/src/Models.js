import React, {useEffect, useState} from 'react'



 function ManufacturesList(){
	const [models, setModels] = useState([])
	const fetchData = async () =>{
		const url = 'http://localhost:8100/api/models/'
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setModels(data.models)
		}
	}

	useEffect(() => {
		fetchData();
	},[])


    return(
		<div>
         <h1>Models</h1>
			<table className ="table table_stripped">
				<thead>
					<tr>
						<th>Name</th>
						<th>Manufacturer</th>
						<th>Picture</th>
					</tr>
				</thead>
				<tbody>
					{models?.map(models => {
						return(
							<tr key={models.id}>
								<td>{models.name}</td>
                                <td>{models.manufacturer.name}</td>
								<td>{models.picture_url}</td>
							</tr>
						)
					})}

				</tbody>
			</table>
		</div>
    )









}


export default ManufacturesList;
