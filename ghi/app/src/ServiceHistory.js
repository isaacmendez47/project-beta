import React, {useEffect, useState} from 'react'

function ServiceHistory(){
	const [appointments, setAppointments] = useState([])

	const fetchData = async () => {
		const url = 'http://localhost:8080/api/appointments/'
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setAppointments(data.appointments)
		}
	}

	useEffect(() => {
	fetchData();
	}, [])

	const cancelAppointment = async(id) => {
		const url = `http//localhost:8080/api/appointments/${id}`
		const response = await fetch(url, {method: "PUT"});
		if (response.ok){
			setAppointments(appointments.filter((appointment) => appointment.id !==id));

		}
	}




    return(
		<div>
         <h1>Service History</h1>
			<table className ="table table_stripped">
				<thead>
					<tr>
						<th>VIN</th>
						<th>Customer Name</th>
						<th>Date</th>
                        <th>Technician</th>
                        <th>Reason</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					{appointments?.map(appointment => {
						return (
							<tr key={appointment.id}>
								<td>{appointment.vin}</td>
								<td>{appointment.customer}</td>
								<td>{appointment.date_time}</td>
								<td>{appointment.technician.id}</td>
								<td>{appointment.reason}</td>
								<td>{appointment.status}</td>


							</tr>
						)
					})}

				</tbody>
			</table>
		</div>
    )









}


export default ServiceHistory;
