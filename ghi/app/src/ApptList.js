import React, {useEffect, useState} from 'react'


function ApptList(){
	const [appointments, setAppointments] = useState([])
	const [search, setSearch] = useState('')


	const handleSearch = event =>{
		setSearch(event.target.value)
	}

	const fetchData = async () => {
		const url = 'http://localhost:8080/api/appointments/'
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setAppointments(data.appointments)
		}
	}

	useEffect(() => {
	fetchData();
	}, [])

	// const cancelAppointment = async(id) => {
	// 	const url = `http//localhost:8080/api/appointments/${id}`
	// 	const response = await fetch(url, {method: "PUT"});
	// 	if (response.ok){
	// 		setAppointments(appointments.filter((appointment) => appointment.id !==id));

	// 	}
	// }





    return(
		<div>
			<input type = 'text' value={search} onChange={handleSearch}></input>
			<button type ='button' onClick={fetchData}>Search by VIN</button>
         <h1>Appointments</h1>
			<table className ="table table_stripped">
				<thead>
					<tr>
						<th>VIN</th>
						<th>Customer Name</th>
						<th>Date</th>
                        <th>Technician</th>
                        <th>Reason</th>
					</tr>
				</thead>
				<tbody>
					{appointments?.map(appointment => {
						const appointmentDate = new Date(appointment.date_time)
						return (
							<tr key={appointment.id}>
								<td>{appointment.vin}</td>
								<td>{appointment.customer}</td>
								<td>{appointmentDate.toLocaleString()}</td>
								<td>{appointment.technician.id}</td>
								<td>{appointment.reason}</td>
								<td><button className='btn btn-danger' >Cancel Appointment</button></td>
								<td><button className='btn btn-success'>Finish Appointment</button></td>

							</tr>
						)
					})}

				</tbody>
			</table>
		</div>
    )









}


export default ApptList;
