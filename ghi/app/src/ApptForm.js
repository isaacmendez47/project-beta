import React, {useEffect, useState} from 'react'



function ApptForm() {

  const [date_time, setDateTime] = useState('');
  const [reason, setReason] = useState('');
  const [status, setStatus] = useState('');
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [technician_id, setTechnicianId] = useState([]);

  const handleDateTimeChange = (event) => {
    const value = event.target.value;
    setDateTime(value);
  }

  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  }

  const handleStatusChange = (event) => {
    const value = event.target.value;
    setStatus(value);
  }

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handleTechnicianIdChange = (event) => {
    const value = event.target.value;
    setTechnicianId(value);
  }


  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};

    data.date_time = date_time;
    data.reason = reason;
    data.status = status;
    data.vin = vin;
    data.customer = customer;
    data.technician_id = technician_id

    const url = `http://localhost:8080/api/appointments/`;
    const response = await fetch(url, {method: "POST", body: JSON.stringify (data)});
    if (response.ok) {
        setDateTime('');
        setReason('');
        setStatus('');
        setVin('');
        setTechnicianId('');
        setCustomer('');
    };
  };







    return(
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create Service Appointment</h1>
              <form onSubmit={handleSubmit}  id="create-tech">
                <div className="form-floating mb-3">
                  <input value={date_time} onChange={handleDateTimeChange} placeholder='First Name'  required type="datetime-local" name="fabric" id="fabric" className="form-control"/>
                  <label>Date</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={reason} onChange={handleReasonChange} placeholder="Last Name" type="text" name="style_name" id="style_name" className="form-control"/>
                  <label htmlFor="style_name">Reason</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={status} onChange={handleStatusChange} placeholder="Status" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Status</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={vin} onChange={handleVinChange}  placeholder="vin" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Vin</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={customer} onChange={handleCustomerChange}  placeholder="customer" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Customer</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={technician_id} onChange={handleTechnicianIdChange}  placeholder="technician_id" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Technician ID</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
        );


}
export default ApptForm;
